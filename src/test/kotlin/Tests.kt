/*
 * baseline-preprocessor
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.swissbib.linked

import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.kafka.common.serialization.StringSerializer
import org.apache.kafka.streams.TopologyTestDriver
import org.apache.kafka.streams.test.ConsumerRecordFactory
import org.apache.logging.log4j.LogManager
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import org.swissbib.SbMetadataDeserializer
import org.swissbib.SbMetadataModel
import org.swissbib.SbMetadataSerializer
import org.swissbib.types.CbsActions
import java.io.File
import java.nio.charset.Charset

class Tests {
    private val log = LogManager.getLogger("TestLogger")
    private val props = KafkaProperties("app.properties", log)
    private val testDriver = TopologyTestDriver(KafkaTopology(props.appProperties, log).build(), props.kafkaProperties)

    private val resourcePath = "src/test/resources/data"
    private fun readFile(fileName: String): String {
        return File("$resourcePath/$fileName").readText(Charset.defaultCharset())
    }

    @Test
    fun testCase1() {
        val factory = ConsumerRecordFactory(
            StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(
            factory.create(
                "sb-all", null, SbMetadataModel().setData(readFile("test1.xml")).setCbsAction(
                    CbsActions.CREATE
                )
            )
        )

        val record: ProducerRecord<String, SbMetadataModel> =
            testDriver.readOutput(
                "sb-all-preprocessed",
                StringDeserializer(),
                SbMetadataDeserializer()
            )
        assertAll("test no enrichment if already present",
            { assertEquals(null, record.key()) },
            { assertEquals(readFile("output1.xml"), record.value().data) }
        )
    }

    @Test
    fun testCase2() {
        val factory = ConsumerRecordFactory(
            StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(
            factory.create(
                "sb-all", null, SbMetadataModel().setData(readFile("test2.xml")).setCbsAction(
                    CbsActions.CREATE
                )
            )
        )
        val record: ProducerRecord<String, SbMetadataModel> =
            testDriver.readOutput(
                "sb-all-preprocessed",
                StringDeserializer(),
                SbMetadataDeserializer()
            )
        assertAll("enrich id in 100 field",
            { assertEquals(null, record.key()) },
            { assertEquals(readFile("output2.xml"), record.value().data) }
        )
    }

    @Test
    fun testCase3() {
        val factory = ConsumerRecordFactory(
            StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(
            factory.create(
                "sb-all", null, SbMetadataModel().setData(readFile("test3.xml")).setCbsAction(
                    CbsActions.CREATE
                )
            )
        )

        val record: ProducerRecord<String, SbMetadataModel> =
            testDriver.readOutput(
                "sb-all-preprocessed",
                StringDeserializer(),
                SbMetadataDeserializer()
            )
        assertAll("field 100\$0 \$ without subfield \$D -> no enrichment",
            { assertEquals(null, record.key()) },
            { assertEquals(readFile("output3.xml"), record.value().data) }
        )
    }
    @Test
    fun testCase4() {
        val factory = ConsumerRecordFactory(
            StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(
            factory.create(
                "sb-all", null, SbMetadataModel().setData(readFile("test4.xml")).setCbsAction(
                    CbsActions.CREATE
                )
            )
        )

        val record: ProducerRecord<String, SbMetadataModel> =
            testDriver.readOutput(
                "sb-all-preprocessed",
                StringDeserializer(),
                SbMetadataDeserializer()
            )
        assertAll("field 110 -> both fields with id -> no enrichment",
            { assertEquals(null, record.key()) },
            { assertEquals(readFile("output4.xml"), record.value().data) }
        )
    }
    @Test
    fun testCase5() {
        val factory = ConsumerRecordFactory(
            StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(
            factory.create(
                "sb-all", null, SbMetadataModel().setData(readFile("test5.xml")).setCbsAction(
                    CbsActions.CREATE
                )
            )
        )

        val record: ProducerRecord<String, SbMetadataModel> =
            testDriver.readOutput(
                "sb-all-preprocessed",
                StringDeserializer(),
                SbMetadataDeserializer()
            )

        assertAll("field 710 has no subfield b",
            { assertEquals(null, record.key()) },
            { assertEquals(readFile("output5.xml"), record.value().data) }
        )
    }



    @Test
    fun testCase6() {
        val factory = ConsumerRecordFactory(
            StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(
            factory.create(
                "sb-all", null, SbMetadataModel().setData(readFile("test6.xml")).setCbsAction(
                    CbsActions.CREATE
                )
            )
        )

        val record: ProducerRecord<String, SbMetadataModel> =
            testDriver.readOutput(
                "sb-all-preprocessed",
                StringDeserializer(),
                SbMetadataDeserializer()
            )
        assertAll("test case 6",
            { assertEquals(null, record.key()) },
            { assertEquals(readFile("output6.xml"), record.value().data) }
        )
    }
    @Test
    fun testCase7() {
        val factory = ConsumerRecordFactory(
            StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(
            factory.create(
                "sb-all", null, SbMetadataModel().setData(readFile("test7.xml")).setCbsAction(
                    CbsActions.CREATE
                )
            )
        )

        val record: ProducerRecord<String, SbMetadataModel> =
            testDriver.readOutput(
                "sb-all-preprocessed",
                StringDeserializer(),
                SbMetadataDeserializer()
            )
        assertAll("test case 7",
            { assertEquals(null, record.key()) },
            { assertEquals(readFile("output7.xml"), record.value().data) }
        )
    }

    @Test
    fun testCase8() {
        val factory = ConsumerRecordFactory(
            StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(
            factory.create(
                "sb-all", null, SbMetadataModel().setData(readFile("test8.xml")).setCbsAction(
                    CbsActions.CREATE
                )
            )
        )

        val record: ProducerRecord<String, SbMetadataModel> =
            testDriver.readOutput(
                "sb-all-preprocessed",
                StringDeserializer(),
                SbMetadataDeserializer()
            )

        assertAll("test case 8",
            { assertEquals(null, record.key()) },
            { assertEquals(readFile("output8.xml"), record.value().data) }
        )
    }
}