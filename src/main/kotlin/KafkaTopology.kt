/*
 * baseline-preprocessor
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.swissbib.linked

import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.Topology
import org.apache.logging.log4j.Logger
import org.marc4j.marc.Record
import org.swissbib.SbMetadataModel
import org.swissbib.types.CbsActions
import java.util.*

class KafkaTopology(private val properties: Properties, private val log: Logger) {
    private val parser = MarcXmlParser(log)

    fun build(): Topology {
        val builder = StreamsBuilder()

        builder
            .stream<String, SbMetadataModel>(properties.getProperty("kafka.topic.source"))
            .mapValues { value -> Pair(value.cbsAction, value.data) }
            .flatMapValues { value -> parseRecord(value) }
            .flatMapValues { value -> process(value) }
            //.flatMapValues { value -> filter(value) }
            .flatMapValues { value -> write(value) }
            // these replacements are necessary to make sure that metafacture can read the marc xml.
            // TODO: Find a way to change the xml writer to do this automatically.
            .mapValues { value -> Pair(value.first, value.second.replace("\n", "")) }
            .mapValues { value -> Pair(value.first, value.second.replace("marc:", "")) }
            .mapValues { value -> Pair(value.first, value.second.replace("<collection xmlns:marc=\"http://www.loc.gov/MARC21/slim\">", "")) }
            .mapValues { value -> Pair(value.first, value.second.replace("</collection>", "")) }
            .mapValues { value -> Pair(value.first, value.second.replace("<record type=\"Bibliographic\">", "<record type=\"Bibliographic\" xmlns=\"http://www.loc.gov/MARC21/slim\">")) }

            .mapValues { value -> SbMetadataModel().setData(value.second).setCbsAction(value.first) }
            .to(properties.getProperty("kafka.topic.sink"))

        return builder.build()
    }

    private fun parseRecord(value: Pair<CbsActions, String>): List<Pair<CbsActions, Record>> {
        val result =  parser.parse(value.second)
        return if (result.isNotEmpty()) {
            listOf(Pair(value.first, result[0]))
        } else {
            emptyList()
        }
    }

    private fun process(value: Pair<CbsActions, Record>): List<Pair<CbsActions, Record>> {
        val result = parser.process(value.second)
        return if (result.isNotEmpty()) {
            listOf(Pair(value.first, result[0]))
        } else {
            emptyList()
        }
    }

    private fun filter(value: Pair<CbsActions, Record>): List<Pair<CbsActions, Record>> {
        val result = parser.filter(value.second)
        return if (result.isNotEmpty()) {
            listOf(Pair(value.first, result[0]))
        } else {
            emptyList()
        }
    }

    private fun write(value: Pair<CbsActions, Record>): List<Pair<CbsActions, String>> {
        val result = parser.write(value.second)
        return if (result.isNotEmpty()) {
            listOf(Pair(value.first, result[0]))
        } else {
            emptyList()
        }
    }

}
