/*
 * baseline-preprocessor
 * Copyright (C) 2020  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.linked

import org.apache.logging.log4j.Logger
import org.xbib.marc.Marc
import org.xbib.marc.MarcField
import org.xbib.marc.MarcFieldAdapter
import org.xbib.marc.transformer.field.MarcFieldTransformer
import org.xbib.marc.transformer.field.MarcFieldTransformers
import org.xbib.marc.xml.MarcXchangeWriter
import java.io.ByteArrayInputStream
import java.io.StringWriter
import java.nio.charset.Charset


class Utility {

    fun extract950Field(source: MarcField): MarcField {
        var tag = ""
        var indicator = ""
        val subfields = mutableListOf<Pair<String, String>>()
        for (subfield in source.subfields.filter { s -> s.id != "B" }) {
            when (subfield.id) {
                "P" -> tag = subfield.value
                "E" -> indicator = subfield.value
                else -> {
                    subfields.add(Pair(subfield.id, subfield.value))
                }
            }
        }
        return buildMarcField(tag, indicator, subfields)
    }

    fun addSubfield(target: MarcField, subfield: Pair<String, String>): MarcField {
        return buildMarcField(target.tag, target.indicator,
            (target.subfields.map { value -> Pair(value.id, value.value) }.toSet() + subfield).toList())
    }

    private fun buildMarcField(tag: String, indicator: String, subfields: List<Pair<String, String>>): MarcField {
        val builder = MarcField.builder()
            .tag(tag)
            .indicator(indicator)
        for (pair in subfields) {
            builder.subfield(pair.first, pair.second)
        }
        return builder.build()
    }

    private fun buildMarcField(tag: String, indicator: String, subfields: MutableMap<String, Set<String>>): MarcField {
        val builder = MarcField.builder()
            .tag(tag)
            .indicator(indicator)
        for (entry in subfields) {
            for (v in entry.value) {
                builder.subfield(entry.key, v)
            }
        }
        return builder.build()
    }
}

class MARCXMLParser(private val log: Logger) {

    private val util = Utility()

    private fun marcField(tag: String, indicator1: String, indicator2: String, subfield: String): MarcField {
        return MarcField.builder().tag(tag).indicator(indicator1 + indicator2).subfield(subfield).build()
    }

    private fun marcField(tag: String, indicator1: String, indicator2: String): MarcField {
        return MarcField.builder().tag(tag).indicator(indicator1 + indicator2).build()
    }

    private fun marcField(tag: String): MarcField {
        return MarcField.builder().tag(tag).build()
    }

    private fun dropTransformer(tag: String): MarcFieldTransformer {
        return MarcFieldTransformer.builder()
            .ignoreIndicator()
            .ignoreSubfieldIds()
            .drop(tag)
            .build()

    }

    private fun createTransformer(from: MarcField, field: MarcField): MarcFieldTransformer {
        return MarcFieldTransformer.builder()
            .ignoreSubfieldIds()
            .fromTo(from, field)
            .build()
    }

    private fun normalizeString(value: String, field: MarcField): String {
        var result = ""
        for (subfield in field.subfields) {
            if (subfield.id == value) {
                result = subfield.value
            }
        }
        result = result.toLowerCase().trim(',', ' ', '.')
        result = ASCIIFolding.foldToASCII(result)
        return result
    }

    fun parse(data: String): String {

        val field950Listener = Field950Adapter(log)
        val out = StringWriter()
        val writer = MarcXchangeWriter(out, false)


        Marc.builder()
            .setInputStream(ByteArrayInputStream(data.toByteArray()))
            .setCharset(Charset.defaultCharset())
            .setMarcListener(field950Listener)
            .build().xmlReader().parse()

        val extractedAuthors = mutableListOf<MarcField>()
        val masterAuthors = mutableListOf<MarcField>()
        for (field in field950Listener.authors) {
            if (field.tag == "950") {
                extractedAuthors.add(util.extract950Field(field))
            } else {
                masterAuthors.add(field)
            }
        }
        log.info(extractedAuthors)
        log.info(masterAuthors)

        val compareValues = mutableListOf<Pair<String, String>>()
        for (field in extractedAuthors) {
            compareValues.add(Pair(normalizeString("a", field), normalizeString("D", field)))
        }

        val enrichedMasterAuthors = mutableListOf<MarcField>()
        for (field in masterAuthors) {
            val value = Pair(normalizeString("a", field), normalizeString("D", field))
            var enrichedMasterAuthor: MarcField? = null
            for (i in compareValues.indices) {
                if (value == compareValues[i]) {
                    val subfield = extractedAuthors[i].subfields.first {v -> v.id == "0"}
                    enrichedMasterAuthor = util.addSubfield(field, Pair(subfield.id, subfield.value))
                }
            }
            if (enrichedMasterAuthor == null) {
                enrichedMasterAuthors.add(field)
            } else {
                enrichedMasterAuthors.add(enrichedMasterAuthor)
            }
        }
        log.info(enrichedMasterAuthors)


        val transformers = MarcFieldTransformers()

        //transformers.add(dropTransformer("100"))
        //transformers.add(dropTransformer("700"))
        for (field in enrichedMasterAuthors) {
            transformers.add(createTransformer(field, field))
        }


        Marc.builder()
            .setInputStream(ByteArrayInputStream(data.toByteArray()))
            .setCharset(Charset.defaultCharset())
            .setMarcFieldTransformers(transformers)
            .setMarcListener(writer)
            .build().xmlReader().parse()

        writer.flush()
        writer.close()
        return out.toString()
    }

}

class Field950Adapter(private val log: Logger) : MarcFieldAdapter() {
    val authors = mutableListOf<MarcField>()

    override fun field(field: MarcField?) {
        if (field == null) return
        // exclude all fields, which contain a subfield t.
        if (field.subfieldIds.contains("t")) return
        when (field.tag) {
            "950" -> {
                // only use 950 fields which actually have a identifier. ignore everything else.
                if (!field.subfieldIds.contains("0")) return
                field.subfields.stream()
                    .filter { value -> value.id == "P" }
                    .forEach { value ->
                        when (value.value) {
                            "100" -> {
                                authors.add(field)
                            }
                            "700" -> {
                                authors.add(field)
                            }
                        }
                    }
            }
            "100" -> authors.add(field)
            "700" -> authors.add(field)
        }

    }

}