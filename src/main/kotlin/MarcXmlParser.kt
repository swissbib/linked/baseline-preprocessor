/*
 * baseline-preprocessor
 * Copyright (C) 2020  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.linked

import org.apache.logging.log4j.Logger
import org.marc4j.MarcException
import org.marc4j.MarcXmlReader
import org.marc4j.MarcXmlWriter
import org.marc4j.marc.DataField
import org.marc4j.marc.Record
import org.marc4j.marc.impl.DataFieldImpl
import org.marc4j.marc.impl.SubfieldImpl
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream


class MarcXmlParser(private val log: Logger) {

    fun parse(data: String): List<Record> {
        return try {
            listOf(MarcXmlReader(ByteArrayInputStream(data.toByteArray())).next())
        } catch (ex: MarcException) {
            log.error(ex.message)
            emptyList()
        }
    }

    fun write(record: Record): List<String> {
        return try {
            val out = ByteArrayOutputStream()
            val writer = MarcXmlWriter(out)
            writer.setIndent(false)
            writer.write(record)
            writer.close()
            // toString(Charset) is introduced in java 10...
            // this uses the default charset which should be UTF-8 anyway.
            listOf(out.toString())
        } catch (ex: MarcException) {
            log.error(ex.message)
            emptyList()
        }

    }

    fun filter(record: Record): List<Record> {
        try {
            record.dataFields
                .removeIf { value -> value.tag == "950" }
        } catch (ex: MarcException) {
            log.error(ex.message)
            return emptyList()
        }
        return listOf(record)
    }

    fun process(record: Record): List<Record> {
        try {
            val authors = mutableListOf<DataField>()
            val organisations = mutableListOf<DataField>()
            val supplementAuthors = mutableListOf<DataField>()
            val supplementOrganisations = mutableListOf<DataField>()
            for (field in record.dataFields) {
                getField(field)?.let {
                    if (it.first == "person") {
                        if (it.second.tag == "950") {
                            supplementAuthors.add(extract950Field(field))
                        } else {
                            authors.add(it.second)
                        }
                    }
                    if (it.first == "organisation") {
                        if (it.second.tag == "950") {
                            supplementOrganisations.add(extract950Field(field))
                        } else {
                            organisations.add(it.second)
                        }
                    }

                }
            }

            for (author in authors) {
                when (author.indicator1) {
                    '0', '1' -> {
                        if (author.hasSubfield('D')) {
                            val values = normalizePair(author, 'a', 'D')
                            for (supAuth in supplementAuthors) {
                                if (supAuth.hasSubfield('D')) {
                                    val supValues = normalizePair(supAuth, 'a', 'D')
                                    val hasEnriched = enrichSubfield(author, supAuth, values, supValues)
                                    if (hasEnriched) break // only add one identifier.
                                } else {
                                    log.error("Slave author without subfield D.")
                                }
                            }
                        } else {
                            log.error("Author without subfield D.")
                        }

                    }
                    else -> log.error("Author with indicator ${author.indicator1}.")
                }
            }

            for (organisation in organisations) {
                when (organisation.indicator1) {
                    '0', '1', '2' -> {
                        if (organisation.hasSubfield('b')) {
                            when (organisation.tag) {
                                "110", "710" -> {
                                    val values = normalizePair(organisation, 'a', 'b')
                                    for (supOrg in supplementOrganisations) {
                                        if (supOrg.tag in listOf("110", "710")) {
                                            if (supOrg.hasSubfield('b')) {
                                                val supValues = normalizePair(supOrg, 'a', 'b')
                                                val hasEnriched = enrichSubfield(organisation, supOrg, values, supValues)
                                                if (hasEnriched) break
                                            } else {
                                                log.error("Organisation has no subfield 'b'.")
                                            }
                                        }
                                    }
                                }
                                "111", "711" -> {
                                    val values = normalizeList(organisation, "acde")
                                    for (supOrg in supplementOrganisations) {
                                        if (supOrg.tag in listOf("110", "710")) {
                                            val supValues = normalizeList(supOrg, "acde")
                                            val hasEnriched = enrichSubfield(organisation, supOrg, values, supValues)
                                            if (hasEnriched) break // only add one identifier.
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else -> {
                        log.error("What to do with indicator ${organisation.indicator1}")
                    }
                }
            }
            return listOf(record)
        } catch (ex: Exception) {

        }
        return emptyList()
    }

    private fun normalizeString(value: String): String {
        var result = value.toLowerCase().trim(',', ' ', '.')
        result = ASCIIFolding.foldToASCII(result)
        return result
    }

    private fun normalizePair(field: DataField, subfield1: Char, subfield2: Char): Pair<String, String> {
       return Pair(normalizeString(field.getSubfield(subfield1).data), normalizeString(field.getSubfield(subfield2).data))
    }

    private fun normalizeList(field: DataField, subfields: String): List<String> {
        val result = mutableListOf<String>()
        for (subfield in subfields) {
            if (field.hasSubfield(subfield)) {
                result += normalizeString(field.getSubfield(subfield).data)
            } else {
                log.error("No subfield $subfield in $field to normalize for list.")
            }
        }
        return result
    }

    private fun enrichSubfield(target: DataField, source: DataField, values: Pair<String, String>, otherValues: Pair<String, String>): Boolean {
        return enrichSubfield(target, source, listOf(values.first, values.second), listOf(otherValues.first, otherValues.second))
    }

    private fun enrichSubfield(target: DataField, source: DataField, values: List<String>, otherValues: List<String>): Boolean {
        return if (values == otherValues) {
            log.info("Found match: $values == $otherValues.")
            log.info("Adding subfield ${source.getSubfield('0')} to data field $target.")
            target.addSubfield(source.getSubfield('0'))
            true
        } else {
            false
        }
    }


    private fun createDataField(tag: String, indicator: String, subfields: List<Pair<Char, String>>): DataField {
        val result = DataFieldImpl(tag, indicator[0], indicator[1])
        for (subfield in subfields) {
            result.addSubfield(SubfieldImpl(subfield.first, subfield.second))
        }
        return result
    }

    private fun extract950Field(field: DataField): DataField {
        var tag = ""
        var indicator = ""
        val subfields = mutableListOf<Pair<Char, String>>()
        for (subfield in field.subfields.filter { s -> s.code != 'B' }) {
            when (subfield.code) {
                'P' -> tag = subfield.data
                'E' -> indicator = subfield.data
                else -> {
                    subfields.add(Pair(subfield.code, subfield.data))
                }
            }
        }
        return createDataField(tag, indicator, subfields)
    }

    private fun getField(field: DataField): Pair<String, DataField>? {
        var dataField: DataField? = null
        var type = ""
        when (field.tag) {
            "950" -> {
                // only use 950 fields which actually have an identifier. ignore everything else.
                if (field.getSubfield('0') == null) return null
                // authors / organisations with subfield t are ignored.
                if (field.getSubfield('t') != null) return null
                field.subfields.stream()
                    .filter { value -> value.code == 'P' }
                    .forEach { value ->
                        when (value.data) {
                            "100", "700" -> { dataField = field; type = "person" }
                            "110", "111", "710", "711" -> { dataField = field; type = "organisation" }
                        }
                    }
            }
            "100", "700" -> {
                if (!field.hasSubfield('0')) {
                    dataField = field
                    type = "person"
                }
            }
            "110", "111", "710", "711" -> {
                if (!field.hasSubfield('0')) {
                    dataField = field
                    type = "organisation"
                }
            }
        }
        return if (dataField == null)
            null
        else
        // the compiler can't tell that the dataField is never null here.
            Pair(type, dataField as DataField)
    }
}